export const featureFlagsList = [
  {
    id: 1,
    active: true,
    created_at: '2018-12-12T22:07:31.401Z',
    updated_at: '2018-12-12T22:07:31.401Z',
    name: 'test flag',
    description: 'flag for tests',
    destroy_path: 'feature_flags/1',
    edit_path: 'feature_flags/1/edit',
  },
];

export const featureFlag = {
  id: 1,
  active: true,
  created_at: '2018-12-12T22:07:31.401Z',
  updated_at: '2018-12-12T22:07:31.401Z',
  name: 'test flag',
  description: 'flag for tests',
  destroy_path: 'feature_flags/1',
  edit_path: 'feature_flags/1/edit',
};
